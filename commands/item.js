const Discord = require('discord.js');

module.exports = {
	name: 'item',
	description: 'Gives you a randome item for 2 Tokens!',
	execute(message, args) {

		if (!args.length) {
			const exampleEmbed = new Discord.MessageEmbed()
				.setColor('#cc0000')
				.setTitle('Item XY')
				.setDescription('Item Description')
				.addFields(
					{ name: 'Damage', value: 'dmg', inline: true },
					{ name: 'Armor', value: 'arm', inline: true },
					{ name: 'Keine Ahnung', value: 'ka', inline: true },
				)
				// .addField('Inline field title', 'Some value here', true)
				.setImage('https://i.imgur.com/wSTFkRM.png');

			return message.channel.send(exampleEmbed);
		}
		return message.channel.send('Items can\'t have arguments!');
	},
};