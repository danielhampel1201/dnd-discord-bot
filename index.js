const fs = require('fs');
const Discord = require('discord.js');
const { prefix, token } = require('./config.json');

const client = new Discord.Client();
client.commands = new Discord.Collection();

const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));

for (const file of commandFiles) {
	const command = require(`./commands/${file}`);
	client.commands.set(command.name, command);
}

client.once('ready', () => {
	console.log('Ready!');
});

client.on('message', message => {
	if (!message.content.startsWith(prefix) || message.author.bot) return;

	if (message.channel.type === 'dm') {
		message.channel.send('I\'m not allowed to send direct messages!');
		return;
	}

	const args = message.content.slice(prefix.length).trim().split(/ +/);
	const commandName = args.shift().toLowerCase();

	if (!client.commands.has(commandName)) {
		message.channel.send('Unknown command. Use "!dnd help" to see a list of valid commands.');
		return;
	}

	const command = client.commands.get(commandName);

	try { command.execute(message, args); }
	catch (error) {
		console.error(error);
		message.reply('There was an error trying to execute that command!');
	}
});

// login to Discord with your app's token
client.login(token);